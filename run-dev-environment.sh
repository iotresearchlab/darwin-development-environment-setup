######
# 
#	This script is used to set up and run development environment for Darwin project.
#	Run docker images for : Kapua, Jenkins, Gerrit, Gitlab.
#
#   Peter Szatmary
#   21.7.2017
#
# NOTICE change directory /home/peter/Dokumenty/ETC/dev-env/ in script with your path
##

# Run Kapua ( ports - 8181, 9200, 1883, 61614, 8080, 8081 )
echo "Running kapua-sql on ports 8181, 3306."
sudo docker run -td --restart always --name kapua-sql -p 8181:8181 -p 3306:3306 -v /home/peter/Dokumenty/ETC/dev-env/kapua-h2-volume:/var/h2/data kapua/kapua-sql
#sudo docker tag kapua/kapua-sql darwin/kapua-sql:1.0

echo "Running kapua-elasticsearch on ports 9200, 9300."
sudo docker run -td --restart always --name kapua-elasticsearch -p 9200:9200 -p 9300:9300 elasticsearch:5.4.0 \
-Ecluster.name=kapua-datastore -Ediscovery.type=single-node -Etransport.host=_site_ -Etransport.ping_schedule=-1 -Etransport.tcp.connect_timeout=30s

echo "Running kapua-broker on ports 1883, 61614."
sudo docker run -td --restart always --name kapua-broker --link kapua-sql:db --link kapua-elasticsearch:es -p 1883:1883 -p 61614:61614 kapua/kapua-broker

echo "Running kapua-console on ports 8080."
sudo docker run -td --restart always --name kapua-console --link kapua-sql:db --link kapua-broker:broker --link kapua-elasticsearch:es -p 8080:8080 kapua/kapua-console

echo "Running kapua-api on ports 8981."
sudo docker run -td --restart always --name kapua-api --link kapua-sql:db --link kapua-broker:broker --link kapua-elasticsearch:es -p 8981:8080 kapua/kapua-api

# Run Jenkins ( ports - 8082, 50000 ) with directory that can be backed up
echo "Running darwin-jenkins on ports 8082, 50 000."
sudo docker run -td --restart always --name darwin-jenkins -p 8082:8080 -p 50000:50000 -v /home/peter/Dokumenty/ETC/dev-env/jenkins-volume:/var/jenkins_home jenkins


# Run SonarQube, run it in sonarqube directory ( ports 8983) // ?
cd /home/peter/Dokumenty/ETC/set-up-dev-env/sonarqube
docker-compose up -d

# Run Artifactory ( ports 8084 )
echo "Running darwin-artifactory on ports 8084."
sudo docker run -td --restart always --name darwin-artifactory -p 8084:8081 -v /home/peter/Dokumenty/ETC/dev-env/artifactory-volume:/var/opt/jfrog/artifactory\ docker.bintray.io/jfrog/artifactory-oss:latest

# Run GitLab ( ports 8085, 8086 )
sudo docker run --name gitlab-postgresql -td --restart always \
    --env 'DB_NAME=gitlabhq_production' \
    --env 'DB_USER=gitlab' --env 'DB_PASS=password' \
    --env 'DB_EXTENSION=pg_trgm' \
    --volume /home/peter/Dokumenty/ETC/dev-env/gitlab/postgresql:/var/lib/postgresql \
    sameersbn/postgresql:9.6-2

sudo docker run --name gitlab-redis -td --restart always \
    --volume /home/peter/Dokumenty/ETC/dev-env/gitlab/redis:/var/lib/redis \
    sameersbn/redis:latest

sudo docker run --name gitlab -td --restart always \
    --link gitlab-postgresql:postgresql --link gitlab-redis:redisio \
    --publish 8086:22 --publish 8085:80 \
    --env 'GITLAB_PORT=8085' --env 'GITLAB_SSH_PORT=8086' \
    --env 'GITLAB_SECRETS_DB_KEY_BASE=long-and-random-alpha-numeric-string' \
    --env 'GITLAB_SECRETS_SECRET_KEY_BASE=long-and-random-alpha-numeric-string' \
    --env 'GITLAB_SECRETS_OTP_KEY_BASE=long-and-random-alpha-numeric-string' \
    --volume /home/peter/Dokumenty/ETC/dev-env/gitlab/gitlab_data:/home/git/data \
    sameersbn/gitlab:9.3.7

# Run LDAP server ( ports 8087, 8022, 389 )
sudo docker run -td --restart always \
  -v /home/peter/Dokumenty/ETC/dev-env/ldap-volume/data/:/var/lib/ldap:rw \
  -v /home/peter/Dokumenty/ETC/dev-env/ldap-volume/slapd.d/:/etc/ldap/slapd.d:rw \
  -e LDAP_DOMAIN=codexa.com \
  -e LDAP_ADMIN_PWD=codeXa123 \
  -e LDAP_ORGANISATION="Codexa s.r.o" \
  -p 389:389 \
  -p 8087:80 \
  -p 8022:22 \
sharaku/ldap;






# ------ should scale , not here but rather in cluster ---------------------------------------------------

# Cassandra : https://hub.docker.com/_/cassandra/
sudo docker run --name darwin-cassandra -td --restart always -p 7000:7000 -p 7001:7001 -p 9042:9042 -p 9160:9160 -p 7199:7199  -v /home/peter/Dokumenty/ETC/dev-env/cassandra-volume:/var/lib/cassandra cassandra:latest

# Coap server
sudo docker run --name darwin-coap-server --restart always -td -p 5683:5683/udp markushx/coap-docker 


# schema registry 8099   
#sudo docker run --name darwin-schema-registry -?td --restart always -p 8099:8000 \
#          -e "SCHEMAREGISTRY_URL=http://192.168.1.137:8099/api/schema-registry" \
#           -e ALLOW_GLOBAL=1 \
#           -e ALLOW_TRANSITIVE=1 \
#           landoop/schema-registry-ui
           
# all in one (registry, confluent platform, connectors, kafka too ?)          
sudo docker run -td  --name confluent-connector-platform -td --restart always \
           -p 3181:3181 -p 3040:3040 -p 7081:7081 \
           -p 7082:7082 -p 7083:7083 -p 7092:7092 -p 8099:8081 \
           -e ZK_PORT=3181 -e WEB_PORT=3040 -e REGISTRY_PORT=8081 \
           -e REST_PORT=7082 -e CONNECT_PORT=7083 -e BROKER_PORT=7092 \
           -e ADV_HOST=192.168.1.137 \
           landoop/fast-data-dev:cp3.1.2


#  ---------------------------------------------------  ---------------------------------------------------

sudo kafka-avro-console-consumer    --zookeeper localhost:2181    --topic coap-topic    --from-beginning;

