# Installation

- statefull cassandra with 3 nodes anf ring, that scales


```
$ kubectl create -f cassandra-service.yaml 
$ kubectl create -f cassandra-statefulset.yaml

```

## Source
- **Install quide:**  https://kubernetes.io/docs/tutorials/stateful-application/cassandra/
- **Scale guide:** https://kubernetes.io/docs/tasks/run-application/scale-stateful-set/
