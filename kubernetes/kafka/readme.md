# Installation

Kafka and zookeeper instalation in Kubernetes that scales.


```shell
$ kubectl apply -f http://central.maven.org/maven2/io/fabric8/ipaas/apps/zookeeper/2.2.168/zookeeper-2.2.168-kubernetes.yml
$ kubectl apply -f http://central.maven.org/maven2/io/fabric8/ipaas/apps/zookeeper-ensemble/2.2.168/zookeeper-ensemble-2.2.168-kubernetes.yml
$ kubectl apply -f http://central.maven.org/maven2/io/fabric8/ipaas/apps/kafka/2.2.168/kafka-2.2.168-kubernetes.yml
$ kubectl apply -f http://central.maven.org/maven2/io/fabric8/ipaas/apps/kafka-manager/2.2.168/kafka-manager-2.2.168-kubernetes.yml
```