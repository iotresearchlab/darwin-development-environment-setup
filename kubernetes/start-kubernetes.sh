# Start kubernetes cluster and its gui proxy on 
# Accessible on 192.168.1.137:8088
# Peter Szatmary

minikube start --vm-driver=virtualbox --memory=13000 --cpus=2 --disk-size=120g
kubectl proxy --address=192.168.1.137 --port=8088 --accept-hosts=.*
